from django.urls import path
from . import views

app_name = 'homeApp'

urlpatterns = [
    path('', views.home, name='home'),
    path('Gallery/', views.gallery, name='gallery'),
    path('Schedule/',views.schedule, name='activities'),
    path('Schedule/delete', views.delete),
    # dilanjutkan ...
]