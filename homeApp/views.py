from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from .forms import ListActivities
from .models import Jadwal

import datetime


# Create your views here.

def home(request):
	return render(request, 'Home.html')

def gallery(request):
	return render(request, 'Gallery.html')

def schedule(request):
	if request.method == "POST":
		fillForm = ListActivities(request.POST)
		if fillForm.is_valid():
			fillForm.save()
			return redirect('homeApp:activities')
	else:
		fillForm = ListActivities()

	task = Jadwal.objects.order_by("tanggal", "waktu")
	time = datetime.datetime.now()
	tasks = {
		'form' : fillForm,
		'tasksname' : task,
		'time' : datetime.datetime.now()
	}
	return render(request, 'Schedule.html', tasks )

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('homeApp:activities')

