# Generated by Django 2.2.6 on 2019-10-05 08:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homeApp', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jadwal',
            old_name='nama_kegiatan',
            new_name='kegiatan',
        ),
    ]
